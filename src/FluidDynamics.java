/*
 Algorithm based on Jos Stam's paper "Real-Time Fluid Dynamics for Games"
 http://www.dgp.toronto.edu/people/stam/reality/Research/pdf/GDC03.pdf
 */

public class FluidDynamics {

    public static void advect(int[] N, int b, double[][] d, double[][] d0, double[][] u, double[][] v, double dt) {
        int i, j, i0, j0, i1, j1;
        double x, y, s0, t0, s1, t1, dt0, dt1;

        dt0 = dt * N[0];
        dt1 = dt * N[1];
        for(i = 1; i <= N[0]; i++) {
            for(j = 1; j <= N[1]; j++) {
                x = i-dt0*u[i][j];
                y = j-dt1*v[i][j];

                if (x < 0.5) x = 0.5;
                if (x > N[0]+0.5) x = N[0] + 0.5;

                i0 = (int)x;
                i1 = i0 + 1;

                if (y < 0.5) y = 0.5;
                if (y > N[1]+0.5) y = N[1] + 0.5;

                j0 = (int)y;
                j1 = j0 + 1;

                s1 = x - i0;
                s0 = 1 - s1;
                t1 = y - j0;
                t0 = 1 - t1;

                d[i][j] = s0 * (t0 * d0[i0][j0] + t1 * d0[i0][j1]) +
                        s1 * (t0 * d0[i1][j0] + t1 * d0[i1][j1]);
            }
        }

        set_bnd(N, b, d);
    }


    public static void diffuse(int[] N, int b, double[][] x, double[][] x0, double diff, double delta) {
        double a = delta * diff * N[0] * N[1];
        int i, j, k;

        for (k = 0; k < 20; k++) {
            for (i = 1; i <= N[0]; i++) {
                for (j = 1; j <= N[1]; j++) {
                    x[i][j] = (x0[i][j] +
                            a * (x[i-1][j] + x[i+1][j] +
                                    x[i][j-1] + x[i][j+1])) / (1 + 4 * a);
                }
            }

            set_bnd(N, b, x);
        }
    }

    public static void project(int N[], double[][] u, double[][] v, double[][] p, double[][] div) {
        int i, j, k;
        double h;

        h = 1.0/N[0]; // don't know about this
        for(i = 1; i <= N[0]; i++) {
            for(j = 1; j <= N[1]; j++) {
                div[i][j] = -0.5 * h * (u[i+1][j] - u[i-1][j] + v[i][j+1] - v[i][j-1]);
                p[i][j] = 0;
            }
        }

        set_bnd(N, 0, div);
        set_bnd(N, 0, p);

        for(k = 0; k < 20; k++) {
            for(i = 1; i <= N[0]; i++) {
                for(j = 1; j <= N[1]; j++) {
                    p[i][j] = (div[i][j] + p[i-1][j] + p[i+1][j] + p[i][j-1] + p[i][j+1]) / 4;
                }
            }
            set_bnd(N, 0, p);
        }

        for(i = 1; i <= N[0]; i++) {
            for(j = 1; j <= N[1]; j++) {
                u[i][j] -= 0.5 * (p[i+1][j] - p[i-1][j]) / h;
                v[i][j] -= 0.5 * (p[i][j+1] - p[i][j-1]) / h;
            }
        }
        set_bnd(N, 1, u); set_bnd(N, 2, v);
    }

    /*
    Fluid is contained in a box with solid walls.
    Horizontal velocity is set to zero on vertical walls and vertical velocity is set to zero on horizontal walls.
    If the function of this is changed, advect() routine should also be modified.
     */
    public static void set_bnd (int[] N, int b, double[][] x) {
        int i;

        for (i = 1; i <= N[1]; i++) {
            x[0][i] = (b == 1) ? -x[1][i] : x[1][i];
            x[N[0] + 1][i] = (b == 1) ? -x[N[0]][i] : x[N[0]][i];
        }
        for (i = 1; i <= N[0]; i++) {
            x[i][0]     = (b==2) ? -x[i][1] : x[i][1];
            x[i][N[1]+1]   = (b==2) ? -x[i][N[1]] : x[i][N[1]];
        }

        x[0][0]             = 0.5*(x[1][0]          +x[0][1]);
        x[0][N[1]+1]        = 0.5*(x[1][N[1]+1]     +x[0][N[1]]);
        x[N[0]+1][0]        = 0.5*(x[N[0]][0]       +x[N[0]+1][1]);
        x[N[0]+1][N[1]+1]   = 0.5*(x[N[0]][N[1]+1]  +x[N[0]+1][N[1]]);
    }
}
