import java.util.HashMap;
import org.fusesource.jansi.Ansi;

public class Renderer {
    private static HashMap<Integer, String> characters = new HashMap<>();
    static {
        characters.put(0, " ");
        characters.put(1, ".");
        characters.put(2, ":");
        characters.put(3, "+");
        characters.put(4, "o");
        characters.put(5, "O");
        characters.put(6, "0");
        characters.put(7, "#");
        characters.put(8, "\u25A5");
        characters.put(9, "\u25A6");
        characters.put(10, "\u2588");
    }

    private static HashMap<Integer, String> colors = new HashMap<>();
    static {
        colors.put(0, "cyan");
        colors.put(1, "red");
    }

    /*
    The matrix consists of integers 11C + V where
    C is the color and V is a value between 0 and 10 (inclusive)

    For example 9: red #
     */

    /**
     * Draws the contents of the matrix in the terminal. Overwrites the last
     * frame, so clear() is not necessary.
     * @param matrix See above
     */
    public static void draw(int[][] matrix) {
        Integer color;
        Integer character;

        // return to row 0 column 0
        System.out.print("\033[H");

        for (int[] line : matrix) {
            for (int i : line) {
                color = i / 11;
                character = i % 11;
                System.out.print(Ansi.ansi().render("@|" + colors.get(color) + " " + characters.get(character) + "|@"));
            }
            System.out.println("");
        }
    }

    /**
     * Clears the terminal, should only be called once.
     */
    public static void clear() {
        System.out.print("\033[H\033[2J");
    }
}
