import org.fusesource.jansi.AnsiConsole;

import java.util.concurrent.TimeUnit;


public class Application {
    private boolean running = true;
    private FluidMatrix fl;
    private double timeStep;
    private long frameTime;

    public Application(String state, double timeStep, long frameTime, double densitySourceStrength, double velocitySourceStrength){
        /*
        Using ANSI/VT100 cursor movement commands to clear screen
        and rewrite existing output.
        http://www.termsys.demon.co.uk/vtansi.htm
         */
        this.timeStep = timeStep;
        this.frameTime = frameTime;

        AnsiConsole.systemInstall();

        setupFluidMatrix(state, densitySourceStrength, velocitySourceStrength);

        Renderer.clear();
    }

    public void execute(String[] args) throws Exception {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run()
            {
                System.out.print("\033[H\033[2J");
                System.out.print("\033[H");
                System.out.println("Shutdown");
                AnsiConsole.systemUninstall();
            }
        });

        while (true) {
            fl.timeStep(timeStep);
//            if(args.length > 0 && args[0].equals("debug"))
//                if(args.length > 1 && args[1].equals("velocity")) {
//                    System.out.println(fl.toVelocityString());
//                } else {
//                    System.out.println(fl.toDensityString());
//                }
//            else
                Renderer.draw(fl.toMatrix());


            TimeUnit.MILLISECONDS.sleep(frameTime);
        }
    }

    void setupFluidMatrix(String state, double denStr, double velStr) {
        switch(state) {
            case "smoke_up":
                fl = new FluidMatrix(new int[] {60, 50});
                fl.addDensitySource(60, 25, denStr);
                fl.addVelocitySource(60, 25, new Vec2d(-velStr, 0.0));
                break;
            default:
                fl = new FluidMatrix(new int[] {60, 60});
                fl.addDensitySource(30, 30, denStr);
        }
    }
}