import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        System.out.println("This program simulates fluid dynamics in a 2D space.");
        System.out.println("It uses algorithms published by Jos Stam in his paper \"Real-Time Fluid Dynamics for Games\"");
        System.out.println("These algorithms are based on the Navier-Stokes equations, but sacrifice physical accuracy\n" +
                "for speed, stability and simplicity. They are more focused on visual quality.");
        Scanner sc = new Scanner(System.in);
        System.out.print("Please enter the strength of density sources (double): ");
        double denStr = sc.nextDouble();
        System.out.print("Please enter the strength of velocity sources (double): ");
        double velStr = sc.nextDouble();

        Application app = new Application(
                (args.length > 0) ? args[0] : "",
                (args.length > 1) ? Double.valueOf(args[1]) : 0.2,
                (args.length > 2) ? Long.valueOf(args[2]) : 25L,
                (denStr == 0) ? 100 : denStr,
                (velStr == 0) ? 200 : velStr
        );

        app.execute(args);
    }
}
