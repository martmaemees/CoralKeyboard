/*
 Algorithm based on Jos Stam's paper "Real-Time Fluid Dynamics for Games"
 http://www.dgp.toronto.edu/people/stam/reality/Research/pdf/GDC03.pdf
 */

public class FluidMatrix {
//    private int size;           // the size of the visible matrix
    private int[] size;           // the size of the visible matrix
//    private int N;              // matrix size with buffer space around the edges
    private int[] N;              // matrix size with buffer space around the edges
    private double[][] rho;     // density
    private double[][] vx;      // velocity x component
    private double[][] vy;      // velocity y component

    private double[][] rhoPrev; // density
    private double[][] vxPrev;  // velocity x component
    private double[][] vyPrev;  // velocity y component

    private final double diff = 0.0001;         // rate of diffusion for density
    private final double viscosity = 0.0001;    // rate of diffusion for velocity (or apparently not?)

    private double[][] densitySources;  // fluid emitted from a point per second
    private double[][] xVelocitySources;       // velocity emitted from a point per second
    private double[][] yVelocitySources;


    /*
     * --CONSTRUCTOR--
     */

    public FluidMatrix(int[] size) {
        this.size = size;
        this.N = new int[] {size[0]+2, size[1] + 2};
        // add additional rows and columns that are out of the screen
        rho = new double[N[0]][N[1]];
        vx = new double[N[0]][N[1]];
        vy = new double[N[0]][N[1]];
        densitySources = new double[N[0]][N[1]];
        xVelocitySources = new double[N[0]][N[1]];
        yVelocitySources = new double[N[0]][N[1]];

        rhoPrev = new double[N[0]][N[1]];
        vxPrev = new double[N[0]][N[1]];
        vyPrev = new double[N[0]][N[1]];
    }


    /*
     * --UPDATE FUNCTIONS CALLED EVERY FRAME--
     */

    public void timeStep(double delta) {
         calculateVelocity(delta);
         calculateDensity(delta);
    }

    private void calculateDensity(double delta) {
        addDensitySources(delta);

        swapDensity();
        FluidDynamics.diffuse(size, 0, rho, rhoPrev, diff, delta);
        swapDensity();
        FluidDynamics.advect(size, 0, rho, rhoPrev, vx, vy, delta);
    }

    private void calculateVelocity(double delta) {
        addVelocitySources(delta);

        swapVelocityX();
        FluidDynamics.diffuse(size, 1, vx, vxPrev, viscosity, delta);
        swapVelocityY();
        FluidDynamics.diffuse(size, 2, vy, vyPrev, viscosity, delta);

        FluidDynamics.project(size, vx, vy, vxPrev, vyPrev);

        swapVelocityX();
        swapVelocityY();
        FluidDynamics.advect(size, 1, vx, vxPrev, vxPrev, vyPrev, delta);
        FluidDynamics.advect(size, 2, vy, vyPrev, vxPrev, vyPrev, delta);

        FluidDynamics.project(size, vx, vy, vxPrev, vyPrev);
    }


    /*
     * --SWAP PREVIOUS AND CURRENT BUFFERS--
     */

    private void swapDensity() {
        double[][] tmp = rho;
        rho = rhoPrev;
        rhoPrev = tmp;
    }

    private void swapVelocityX() {
        double[][] tmp = vx;
        vx = vxPrev;
        vxPrev = tmp;
    }

    private void swapVelocityY() {
        double[][] tmp = vy;
        vy = vyPrev;
        vyPrev = tmp;
    }


    /*
     * --INTERACTIONS WITH BUFFER CONTENTS--
     */

    public void addDensitySource(int x, int y, double value) {
        if(x < 0 || x >= densitySources.length || y < 0 || y > densitySources[0].length) {
            System.err.println("Tried to add a density source that is not in the correct boundary.");
            return;
        }
        densitySources[x][y] = value;
    }

    public void addVelocitySource(int x, int y, Vec2d value) {
        if(x < 0 || x >= densitySources.length || y < 0 || y > densitySources[0].length) {
            System.err.println("Tried to add a density source that is not in the correct boundary.");
            return;
        }
        xVelocitySources[x][y] = value.x;
        yVelocitySources[x][y] = value.y;
    }

    public void clearDensitySource(int x, int y) {
        if(x < 0 || x >= densitySources.length || y < 0 || y > densitySources[0].length) {
            System.err.println("Tried to clear a density source that is not in the correct boundary.");
            return;
        }
        densitySources[x][y] = 0;
    }

    public void clearVelocitySource(int x, int y) {
        if(x < 0 || x >= densitySources.length || y < 0 || y > densitySources[0].length) {
            System.err.println("Tried to clear a velocity source that is not in the correct boundary.");
            return;
        }
        xVelocitySources[x][y] = 0;
        yVelocitySources[x][y] = 0;
    }

    private void addDensitySources(double delta) {
        int i, j;
        for (i = 0; i < N[0]; i++) {
            for (j = 0; j < N[1]; j++) {
                rho[i][j] += densitySources[i][j] * delta;
            }
        }
    }

    private void addVelocitySources(double delta) {
        int i, j;
        for(i = 0; i < N[0]; i++) {
            for(j = 0; j < N[1]; j++) {
                vx[i][j] += xVelocitySources[i][j] * delta;
                vy[i][j] += yVelocitySources[i][j] * delta;
            }
        }
    }

    /*
     * --MATRIX OUTPUTS FOR VISUALIZATION--
     */

    public String toDensityString() {
        StringBuilder sb = new StringBuilder(200);

        for (double[] line : rho) {
            for (double p : line) {
                sb.append(String.format("%-6s", String.valueOf(Math.round(p*1000)/1000.0f)));
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public String toVelocityString() {
        StringBuilder sb = new StringBuilder(200);

        for(int i = 0; i < vx.length; i++) {
            for(int j = 0; j < vx[0].length; j++) {
                sb.append(String.format("(%-5s, %-5s)", String.valueOf(Math.round(vx[i][j]*100)/100.0f),
                          String.valueOf(Math.round(vy[i][j]*100)/100.0f)));
//                sb.append("(" + vxPrev[i][j] + " "  + vy[i][j] + ")");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public int[][] toMatrix() {
        int[][] mat = new int[N[0]][N[1]];
        int i, j;


//        for (i = 1; i <= size[0]; i++) {
//            for (j = 1; j <= size[1]; j++) {
//                mat[i-1][j-1] = (int)Math.round(Math.pow(Math.min(rho[i][j], 2)/2, 0.6) * 10);
////                mat[i-1][j-1] = (int)Math.round(Math.min(rho[i][j], 2)/2 * 10);
//            }
//        }


        for (i = 0; i < N[0]; i++) {
            for (j = 0; j < N[1]; j++) {
                mat[i][j] = (int)Math.round(Math.pow(Math.min(rho[i][j], 2)/2, 0.6) * 10);
//                mat[i][j] = (int)Math.round(Math.min(rho[i][j], 2)/2 * 10);
            }
        }


        return mat;
    }


    /*
     * --GETTERS AND SETTERS--
     */

    public double[][] getDensitySources() { return densitySources; }

    public void setDensitySources(double[][] value) { densitySources = value; }

    public double[][] getXVelocitySources() { return xVelocitySources; }

    public void setXVelocitySources(double[][] value) { xVelocitySources = value; }

    public double[][] getYVelocitySources() { return yVelocitySources; }

    public void setYVelocitySources(double[][] value) { yVelocitySources = value; }

    public void setRho(int x, int y, double value) {
        rho[x][y] = value;
    }

    public double[][] getRho() { return rho; }

    public void setVelocity(int x, int y, double xVel, double yVel) {
        if(x < 0 || x >= size[0] || y < 0 || y >= size[1]) {
            return;
        }
        vx[x][y] = xVel;
        vy[x][y] = yVel;
    }

}
